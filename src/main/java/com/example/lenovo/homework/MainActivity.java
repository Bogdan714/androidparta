package com.example.lenovo.homework;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.text)
    TextView field;

    @BindView(R.id.clearText)
    Button clear;

    @BindView(R.id.printText)
    Button print;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.clearText)
    public void onClickClear(){
        field.setText("");
    }

    @OnClick(R.id.printText)
    public void onClickPrint(){
        field.setText("Hello World");
    }

}
